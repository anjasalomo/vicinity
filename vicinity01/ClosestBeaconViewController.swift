
import UIKit
import CoreLocation

//CLLoc.MngDel um der Klasse zu sagen, welche Delegate Methoden sie zur Verfuegung hat
class ClosestBeaconViewController: UIViewController, CLLocationManagerDelegate {
    
    // koordiniert alle Location Dienste
    // ist eine Instanz vom CLLocationManager
    let locationManager = CLLocationManager()
    
    // um die UUID der Beacons anzugeben, welche die App ansprechen sollen
    let region = CLBeaconRegion (proximityUUID: NSUUID(UUIDString: "8492E75F-4FD6-469D-B132-043FE94921D8"), identifier: "Estimotes")
    let colors =  [
        4554: UIColor(red: 72/255, green: 61/255, blue: 139/255, alpha: 1), //MINOR Nummer angeben!!
        10373: UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.delegate = self
        
        //wenn der Zugriff nicht erlaubt, dann fragen
        if(CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedWhenInUse){
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startRangingBeaconsInRegion(region)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!)
    {
        println(beacons)
        let knownBeacons = beacons.filter
            {
                //$0 = jedes Element des Arrays
                //alle beacons im Array bei denen die Proximity bekannt ist (also nicht unknown)
                $0.proximity != CLProximity.Unknown
        }
        if(knownBeacons.count > 0)
        {
            // = das erste Beacon des knownBeacons Array
            let closestBeacon = knownBeacons[0] as CLBeacon
            self.view.backgroundColor = self.colors[closestBeacon.minor.integerValue]
        }
        
    }
    
}


